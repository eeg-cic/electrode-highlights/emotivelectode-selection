clear
clc

pathProg = uigetdir;

if (~isdeployed)
    pathPrograms=pathProg;
    addpath(genpath(pathPrograms));
end

Picture = 'schema256elecs-257';
im = imread([pathProg, filesep, 'schema14elecs-14.png']); % image to display (B&W)
M = importdata([pathProg, filesep, 'schema14elecs-14.mat']); % matrix with zones coordinates pixel by pixel
elecNames = csvimport([pathProg, filesep, 'csv14ElecCorresNames.csv']);

ColorPixel = [249 137 31]; % couleur en rgb

% List of files to load
fileList = uipickfiles('FilterSpec', pathProg);
NbFiles = length(fileList);

for iter = 1:length(fileList)
    fileName = char(fileList{iter});
    ifCompare = strfind(fileName, 'compare');
    ifAsymmetry = strfind(fileName, 'asymmetry');
    ifWholeBrain = strfind(fileName, 'wholebrain');
    
    csvRAW = csvimport(fileName); % import data
    
    if ~isempty(ifCompare) % take col 2, elec alone
        columnToTake = 2;
        elecColumn = csvRAW(2:end, columnToTake); % isolate the column of interest
        elecsSelection = elecColumn;
        disp('ifCompare');
        
    elseif ~isempty(ifAsymmetry) % take col 1, pair
        columnToTake = 1;
        elecColumn = csvRAW(2:end, columnToTake); % isolate the column of interest
        IndexCol = strfind(elecColumn, '-'); % if pair
        if length(IndexCol) == 0
            % Save the final cluster
            disp('ifAsymmetry');
            im2 = im;
            image(im2)
            pause(0.5)
            imwrite(im2,[fileName, '.png']); % sauvegarde image
            im = imread([pathProg, filesep, 'schema14elecs-14.png']); % image to display (B&W)
            clear elecsSelection
            clear elecColumn
            clear elecsToShow
            clear elecLeft
            clear elecRight
            continue
        else
            for ii=1:length(IndexCol)
                elecRight{ii} = elecColumn{ii}(2:IndexCol{ii}-1);
                elecLeft{ii} = elecColumn{ii}(IndexCol{ii}+1:end-1);
            end
            elecsSelection = [transpose(elecRight);transpose(elecLeft)];
        end
        disp('ifAsymmetry');
        
    else % take col 1, elec alone
        columnToTake = 1;
        elecColumn = csvRAW(2:end, columnToTake); % isolate the column of interest
        elecsSelection = elecColumn;
        disp('ifWholeBrain');
    end
    
    % retrouver le num�ro d'index correspondant aux elec qui ont un nom
    % avec lettres
    for ii = 1:length(elecsSelection)
        elecsSelection{ii} = strrep(elecsSelection{ii}, '"', '');
        IndexElec = find(strcmp(elecNames, elecsSelection{ii}));
        elecsToShow(ii) = IndexElec;
    end
    
    % 
    if length(elecsSelection)>0
        for jj = 1:length(elecsToShow)
            [xxx,yyy] = find(M==elecsToShow(jj)); % count = num�ro d'electrode a afficher
            for it = 1:size(xxx)
                im(xxx(it),yyy(it),:) = ColorPixel; % Color the zone
            end
        end
    end

    % Save the final cluster
    im2 = im;
    image(im2)
    pause(0.5)
    imwrite(im2,[fileName, '.png']); % sauvegarde image
    im = imread([pathProg, filesep, 'schema14elecs-14.png']); % image to display (B&W)
    clear elecsSelection
    clear elecColumn
    clear elecsToShow
    clear elecLeft
    clear elecRight
end
